<?php

// ----------------------------------------------------------------------------
// markItUp! BBCode Parser
// v 1.0.6
// Dual licensed under the MIT and GPL licenses.
// ----------------------------------------------------------------------------
// Copyright (C) 2009 Jay Salvat
// http://www.jaysalvat.com/
// http://markitup.jaysalvat.com/
// 
// Copyright (C) 2011 Martijn van der Kleijn
// http://www.vanderkleijn.net/
// Parser adapted and altered to be more generic and robust.
// ----------------------------------------------------------------------------
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// ----------------------------------------------------------------------------
// Thanks to Arialdo Martini, Mustafa Dindar for feedbacks.
// ----------------------------------------------------------------------------

define("EMOTICONS_DIR", PLUGINS_URI.'bbcode/images/emoticons/');

function BBCode2Html($text) {
    $text = trim($text);

    // Create automatic links
    $text = preg_replace('#(?<=[\s\]\)])(<)?(\[)?(\()?([\'"]?)(https?|ftp|news){1}://([\w\-]+\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^\s\[]*[^\s.,?!\[;:-]?)?)\4(?(3)(\)))(?(2)(\]))(?(1)(>))(?![^\s]*\[/(?:url|img)\])#ie', 'stripslashes(\'$1$2$3$4\').handle_url_tag(\'$5://$6\', \'$5://$6\', true).stripslashes(\'$4$10$11$12\')', $text);
    $text = preg_replace('#(?<=[\s\]\)])(<)?(\[)?(\()?([\'"]?)(www|ftp)\.(([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^\s\[]*[^\s.,?!\[;:-])?)\4(?(3)(\)))(?(2)(\]))(?(1)(>))(?![^\s]*\[/(?:url|img)\])#ie', 'stripslashes(\'$1$2$3$4\').handle_url_tag(\'$5.$6\', \'$5.$6\', true).stripslashes(\'$4$10$11$12\')', $text);

    // BBCode [code]
    if (!function_exists('escape')) {

        function escape($s) {
            global $text;
            $text = strip_tags($text);
            $code = trim($s[1]);
            $code = htmlspecialchars($code);
            $code = str_replace("[", "&#91;", $code);
            $code = str_replace("]", "&#93;", $code);
            return '<pre><code>' . $code . '</code></pre>';
        }

    }
    $text = preg_replace_callback('/\[code\](.*?)\[\/code\]/ms', "escape", $text);

    // quotes
    $text = preg_replace('#\[quote=(&quot;|"|\'|)(.*?)\\1\]#e', '"<cite>".str_replace(array(\'[\', \'\\"\'), array(\'&#91;\', \'"\'), \'$2\')." ".\'wrote\'.":</cite><blockquote>"', $text);
    $text = preg_replace('#\[quote\]\s*#', '<blockquote>', $text);
    $text = preg_replace('#\s*\[\/quote\]#S', '</blockquote>', $text);

    // BBCode to find...
    $in = array(
        '/\[b\](.*?)\[\/b\]/ms',
        '/\[i\](.*?)\[\/i\]/ms',
        '/\[u\](.*?)\[\/u\]/ms',
        '/\[img\](.*?)\[\/img\]/ms',
        '/\[email\](.*?)\[\/email\]/ms',
        '/\[url\="?(.*?)"?\](.*?)\[\/url\]/ms',
        '/\[size\="?(.*?)"?\](.*?)\[\/size\]/ms',
        '/\[color\="?(.*?)"?\](.*?)\[\/color\]/ms',
        '/\[list\=(.*?)\](.*?)\[\/list\]/ms',
        '/\[list\](.*?)\[\/list\]/ms',
        '/\[\*\]\s?(.*?)\n/ms'
    );
    // And replace them by...
    $out = array(
        '<strong>\1</strong>',
        '<em>\1</em>',
        '<u>\1</u>',
        '<img src="\1" alt="\1" />',
        '<a href="mailto:\1">\1</a>',
        '<a href="\1">\2</a>',
        '<span style="font-size:\1%">\2</span>',
        '<span style="color:\1">\2</span>',
        '<ol start="\1">\2</ol>',
        '<ul>\1</ul>',
        '<li>\1</li>'
    );
    $text = preg_replace($in, $out, $text);

    // paragraphs
    $text = str_replace("\r", "", $text);
    $text = "<p>" . preg_replace("/(\n){2,}/", "</p><p>", $text) . "</p>";
    $text = nl2br($text);

    // clean some tags to remain strict
    // not very elegant, but it works. No time to do better ;)
    if (!function_exists('removeBr')) {

        function removeBr($s) {
            return str_replace("<br />", "", $s[0]);
        }

    }
    $text = preg_replace_callback('/<pre>(.*?)<\/pre>/ms', "removeBr", $text);
    $text = preg_replace('/<p><pre>(.*?)<\/pre><\/p>/ms', "<pre>\\1</pre>", $text);

    $text = preg_replace_callback('/<ul>(.*?)<\/ul>/ms', "removeBr", $text);
    $text = preg_replace('/<p><ul>(.*?)<\/ul><\/p>/ms', "<ul>\\1</ul>", $text);

    // Smilies to replace
    $smilies = array(
        ':)' => 'smile.png',
        ':-)' => 'smile.png',
        '=)' => 'smile.png',
        ':|' => 'neutral.png',
        '=|' => 'neutral.png',
        ':(' => 'sad.png',
        ':-(' => 'sad.png',
        '=(' => 'sad.png',
        ':D' => 'big_smile.png',
        '=D' => 'big_smile.png',
        ':o' => 'yikes.png',
        ':O' => 'yikes.png',
        ':-o' => 'yikes.png',
        ':-O' => 'yikes.png',
        ';)' => 'wink.png',
        ';-)' => 'wink.png',
        ':/' => 'hmm.png',
        ':-/' => 'hmm.png',
        '=/' => 'hmm.png',
        ':p' => 'tongue.png',
        ':P' => 'tongue.png',
        ':-p' => 'tongue.png',
        ':-P' => 'tongue.png',
        ':lol:' => 'lol.png',
        ':mad:' => 'mad.png',
        ':rolleyes:' => 'roll.png',
        ':cool:' => 'cool.png'
    );

    foreach ($smilies as $smiley_text => $smiley_img) {
        if (strpos($text, $smiley_text) !== false) {
            $text = preg_replace("#(?<=[>\s])" . preg_quote($smiley_text, '#') . "(?=\W)#m", '<img src="' . EMOTICONS_DIR . $smiley_img . '" alt="' . substr($smiley_img, 0, strrpos($smiley_img, '.')) . '" />', $text);
        }
    }

    return $text;
}

//
// Truncate URL if longer than 55 characters (add http:// or ftp:// if missing)
// 
// Function copied from PunBB, then modified.
//
function handle_url_tag($url, $link = '', $bbcode = false) {
    $full_url = str_replace(array(' ', '\'', '`', '"'), array('%20', '', '', ''), $url);
    if (strpos($url, 'www.') === 0)   // If it starts with www, we add http://
        $full_url = 'http://' . $full_url;
    else if (strpos($url, 'ftp.') === 0) // Else if it starts with ftp, we add ftp://
        $full_url = 'ftp://' . $full_url;
    else if (!preg_match('#^([a-z0-9]{3,6})://#', $url))  // Else if it doesn't start with abcdef://, we add http://
        $full_url = 'http://' . $full_url;

    // Ok, not very pretty :-)
    if (!$bbcode)
        $link = ($link == '' || $link == $url) ? ((utf8_strlen($url) > 55) ? utf8_substr($url, 0, 39) . ' &#133; ' . utf8_substr($url, -10) : $url) : stripslashes($link);

    if ($bbcode) {
        /*(if ($full_url == $link)
            return '[url]' . $link . '[/url]';
        else*/
            return '[url=' . $full_url . ']' . $link . '[/url]';
    }
    else
        return '<a href="' . $full_url . '">' . $link . '</a>';
}

?>