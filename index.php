<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2009-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of Wolf CMS. Wolf CMS is licensed under the GNU GPLv3 license.
 * Please see license.txt for the full license text.
 */

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

/**
 * The BBCode plugin provides a Filter that uses a BBCode parser.
 *
 * @package Plugins
 * @subpackage bbcode
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2011
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 */

Plugin::setInfos(array(
    'id'          => 'bbcode',
    'title'       => __('BBCode filter'),
    'description' => __('Allows you to use the BBCode text filter.'),
    'version'     => '1.0.0',
    'website'     => 'http://www.vanderkleijn.net/',
    'update_url'  => 'http://www.vanderkleijn.net/plugins.xml',
    'require_wolf_version' => '0.7.3'
));

Filter::add('bbcode', 'bbcode/filter_bbcode.php');
Plugin::addController('bbcode', __('BBCode'), 'admin_edit', false);
Plugin::addJavascript('bbcode', 'bbcode.php');