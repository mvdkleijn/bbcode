<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2011 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of Wolf CMS. Wolf CMS is licensed under the GNU GPLv3 license.
 * Please see license.txt for the full license text.
 */

/**
 * The BBCode plugin provides a Filter that uses a BBCode parser.
 *
 * @package Plugins
 * @subpackage bbcode
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2011
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 */


/* Security measure */
if (!defined('IN_CMS')) { exit(); }

/**
 * The main controller for the BBCode plugin.
 */
class BbcodeController extends PluginController {

    public function __construct() { }

    public function preview() {
        require_once('bbcodeParser.php');
        $text = $_POST['data'];

        echo BBCode2Html($text);
    }
}