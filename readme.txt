== USAGE

1) Download and unzip into Wolf CMS plugins folder. The plugin's foldername
   should be "bbcode".

2) Login to your Wolf CMS administration section and click enable for the plugin.

3) Select the BBCode filter when editing a page and start using it.

== LICENSE:

Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>

This file is part of the BBCode plugin for Wolf CMS.

Wolf CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wolf CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wolf CMS.  If not, see <http://www.gnu.org/licenses/>.

Wolf CMS has made an exception to the GNU General Public License for plugins.
See exception.txt for details and the full text.